/**
 * faça um algoritmo que receba um número entre 1 e 21 e retorne o fatorial
 *
 */
var readlineSync = require("readline-sync");

var numero = Number (readlineSync.question("Digite um numero entre 1 e 21 "));

function fatorial(n) {
    let result = 1;
    for (let i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}

if(numero >= 1 && numero <=21){
    console.log(`O fatorial de ${numero} é ${fatorial(numero)}`);
}else{
    console.log("número inválido!")
}