/**
 * faça um código que retorne o nome e cargo do trabalhador com maior salário, use o array de objetos abaixo
 */

const trabalhadores = [
	{
		nome: "José",
		salario_bruto: 3950.23,
		cargo: "Analista de sistemas jr",
	},
	{
		nome: "Maria",
		salario_bruto: 5900,
		cargo: "Analista de sistemas pleno",
	},
	{
		nome: "João",
		salario_bruto: 2500,
		cargo: "estagiário",
	},
	{
		nome: "Sérgio",
		salario_bruto: 3951.5,
		cargo: "Consultor financeiro",
	},
];

let trabalhadoresSorteados = trabalhadores.sort((a,b) => a.salario_bruto + b.salario_bruto)
console.log(trabalhadoresSorteados[0])
// let trabalhadorEncontrado = {};
// let ultimoSalario = 0
// for(let item in trabalhadores){
// 	let trabalhador = trabalhadores[item]
// 	if(trabalhador.salario_bruto > ultimoSalario){
// 		trabalhadorEncontrado = trabalhador;
// 	}
// }

// console.log(trabalhadorEncontrado)