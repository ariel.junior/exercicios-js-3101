/**
 * faça um código que calcule a porcentagem de um número e retorne o valor encontrado:
 * o código deve:
 * - receber o valor total;
 * - receber a porcentagem (de 1 a 100);
 * - retornar o valor calculado;
 */
var readlineSync = require("readline-sync");

var valorTotal = Number(readlineSync.question("Digite um numero "));
var porcentagem = Number(readlineSync.question("Digite a porcentagem "));

function calcularPorcentagem(valorTotal, porcentagem) {
  return (porcentagem * valorTotal) / 100;
}

console.log(
  `O valor calculado é ${calcularPorcentagem(valorTotal, porcentagem)}`
);
