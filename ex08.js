/**
 * faça um algoritmo que receba um número e retorne a tabuada desse número de acordo com o exemplo abaixo:
 *
 * 1 x 1 = 1
 * 1 x 2 = 2
 * 1 x 3 = 3
 * 1 x 4 = 4
 * 1 x 5 = 5
 * 1 x 6 = 6
 * 1 x 7 = 7
 * 1 x 8 = 8
 * 1 x 9 = 9
 * 1 x 10 = 10
 */

var readlineSync = require("readline-sync");

var num = Number(readlineSync.question("Digite um numero "));

function tabuada(num) {
  for (let i = 1; i <= 10; i++) {
    console.log(`${num} x ${i} = ${num * i}`);
  }
}
tabuada(num);
