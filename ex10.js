/**
 * Faça um algoritmo que retorne o valor total de itens na lista de carrinhoDeCompras
 */

const carrinhoDeCompras = [
  {
    nome: "Ventilador",
    preco: 100.5,
  },
  {
    nome: "Carrinho de controle remoto",
    preco: 500.0,
  },
  {
    nome: "Headphone JBL plus X",
    preco: 50.0,
  },
  {
    nome: "Apple Earpods",
    preco: 1500.0,
  },
];

const valorTotal = carrinhoDeCompras.reduce(
  (total, item) => total + item.preco,
  0
);

const valorMoeda = valorTotal.toLocaleString("pt-BR", {
  minimumFractionDigits: 2,
  style: "currency",
  currency: "BRL",
});

console.log(`O valor total do carinho é ${valorMoeda}`);
