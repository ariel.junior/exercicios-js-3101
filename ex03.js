/**
 * Faça um programa que receba 3 número inteiros ou decimais e calcule a média, 
 * valide e retorne uma mensagem para cada situação.
 * 
 * - se a média for <= 1, retorne “Aluno reprovado”;
 * - se a média for >= 2 e <= 4, retorne “Aluno em recuperação”;
 * - se a média for >= 5 e <= 7, retorne “Aluno aprovado”;
 * - se a média for > 7, retorne “Aluno aprovado com ótimo aproveitamento”;
 *
 * OBS: para calcular a média some todos os números e divide pelo total de números.
 */

var readlineSync = require("readline-sync");

const numero1 = Number(readlineSync.question("primeira nota: "));
const numero2 = Number(readlineSync.question("segunda nota: "));
const numero3 = Number(readlineSync.question("terceira nota: "));

const media = (numero1 + numero2 + numero3) / 3;

if(media > 7){
    console.log('Aluno aprovado com ótimo aproveitamento')
}else if(media >= 5){
    console.log('Aluno aprovado')
}else if(media >= 2){
    console.log('Aluno em recuperação')
}else{
    console.log('Aluno reprovado')
}

console.log(`média ${media}`)