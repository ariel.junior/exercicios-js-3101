/**
 * Faça um algoritmo que retorne o aluno de índice 2 
 * retorne o texto "verdadeiro" caso seu nome seja Maria.
 * retorne o texto "falso" caso seu nome seja diferente de Maria
 */
const alunos = [
	{
		nome: "José",
		curso: "Técnico em enfermagem",
	},
	{
		nome: "Maria",
		curso: "Ciência da computação",
	},
	{
		nome: "João",
        curso: "Matemática",
	},
	{
		nome: "Sérgio",
        curso: "Engenharia química"
	}
];

const { nome } = alunos[2];

let result = "falso";

if(nome.toUpperCase() === "MARIA"){
	result = "verdadeiro"
}

console.log(result);

