/**
 * Faça um programa que receba um número e retorne se ele é par ou impar
 */
const input = require("readline-sync");

let numero = Number(input.question("Digite seu numero:"))

const resultado = numero % 2 === 0 ? 'par' : 'impar';

console.log(`O número ${numero} é ${resultado}`)