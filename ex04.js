/**
 * Faça um programa que receba o mês e retorne o total de dias
 *
 * janeiro = 31 dias
 * fevereiro = 28 dias
 * março = 31 dias
 * abril = 30 dias
 * maio = 31 dias
 * junho = 30 dias
 * julho = 31 dias
 * agosto = 31 dias
 * setembro = 30 dias
 * outubro = 31 dias
 * novembro = 30 dias
 * dezembro = 31 dias
 */

const input = require("readline-sync");

const meses = {
    janeiro: 0,
    fevereiro: 1,
    marco: 2,
    abril: 3,
    maio: 4,
    junho: 5,
    julho: 6,
    agosto: 7,
    setembro: 8,
    outubro: 9,
    novembro: 10,
    dezembro: 11
}
let ano = 2023
let mes = String(input.question("Digite o nome do mês:"))
let mesIndex = meses[mes]
let resultado = new Date(ano, mesIndex, 0).getDate()

console.log(`O mês ${mes} tem ${resultado} dias`)