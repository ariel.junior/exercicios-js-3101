/**
 * Faça um programa que receba o nome e idade do usuário e retorne:
 * - o nome do usuário;
 * - se ele é menor ou maior de idade;
 */
const input=require("readline-sync")

const nome = input.question("Digite seu nome:")
const idade = Number(input.question("Digite sua idade:"))

console.log(`
    Usuário: ${nome}\n\n
    Status: ${idade >=18 ? "Maior de idade":"Menor de idade"}
`)
